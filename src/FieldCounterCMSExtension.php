<?php

namespace Hestec\FieldCounter;

use SilverStripe\Admin\LeftAndMainExtension;
use SilverStripe\View\Requirements;

class FieldCounterCMSExtension extends LeftAndMainExtension
{

    public function init()
    {
        parent::init();

        Requirements::css('hestec/silverstripe-fieldcounter: client/css/counter.css');
        Requirements::javascript('hestec/silverstripe-fieldcounter: client/js/counter.js');

    }

}

# Silverstripe FieldCounter
Shows a field counter on specified fields in the SilverStripe CMS

This is an update of the PlatoCreative fieldcounter: https://github.com/PlatoCreative/silverstripe-fieldcounter
This repository has an updated folder structure and moves the css and js to the resources folder.

### Usage
To add the field counter to a Silverstripe field in the CMS you need to add:
```php
TextField::('myfield')->setAttribute('data-fieldcounter', 140);
```
If you require different limits change the 140.
Note: Adding this to a TextareaField prevents new lines.

If you want to prevent more text than your limit, use the built in HTML attribute `maxlength` e.g. 
```php
TextField::('myfield')->setAttribute('maxlength', 140);
```

### Install
`$ composer require hestec/silverstripe-fieldcounter:1.*`
